--------------------------------------------------------
--  DDL for User PAY_SYSTEM
--------------------------------------------------------

  CREATE USER pay_system IDENTIFIED BY pass4paysystem;
--------------------------------------------------------
--  GRANT Privileges TO PAY_SYSTEM
--------------------------------------------------------

  GRANT create session TO pay_system;
  GRANT create table TO pay_system;
  GRANT create view TO pay_system;
  GRANT create any trigger TO pay_system;
  GRANT create any procedure TO pay_system;
  GRANT create sequence TO pay_system;
  GRANT create synonym TO pay_system;
--------------------------------------------------------
--  DDL for Table ACCOUNT_BALANCES
--------------------------------------------------------

  CREATE TABLE "PAY_SYSTEM"."ACCOUNT_BALANCES" 
   (	
    "BALANCE_ID" NUMBER(19,0), 
	"AMOUNT" NUMBER, 
	"ACCOUNT_ID" NUMBER(19,0)
   );

   COMMENT ON COLUMN "PAY_SYSTEM"."ACCOUNT_BALANCES"."BALANCE_ID" IS 'Primary key of account_balances table';
   COMMENT ON COLUMN "PAY_SYSTEM"."ACCOUNT_BALANCES"."AMOUNT" IS 'Total balance of the account';
   COMMENT ON COLUMN "PAY_SYSTEM"."ACCOUNT_BALANCES"."ACCOUNT_ID" IS 'Account id of the account';
--------------------------------------------------------
--  DDL for Table ACCOUNTS
--------------------------------------------------------

  CREATE TABLE "PAY_SYSTEM"."ACCOUNTS" 
   (	
    "ACCOUNT_ID" NUMBER(19,0), 
	"CUSTOMER_ID" NUMBER(19,0)
   );

   COMMENT ON COLUMN "PAY_SYSTEM"."ACCOUNTS"."ACCOUNT_ID" IS 'Primary key of the accounts table. It represents the specific number of the account.';
   COMMENT ON COLUMN "PAY_SYSTEM"."ACCOUNTS"."CUSTOMER_ID" IS 'Customer id of the account owner';
--------------------------------------------------------
--  DDL for Table CUSTOMERS
--------------------------------------------------------

  CREATE TABLE "PAY_SYSTEM"."CUSTOMERS" 
   (
    "CUSTOMER_ID" NUMBER(19,0), 
	"FIRST_NAME" VARCHAR2(20), 
	"LAST_NAME" VARCHAR2(25), 
	"FATHER_NAME" VARCHAR2(25), 
	"EMAIL" VARCHAR2(25), 
	"TEL" VARCHAR2(11)
   );
   
   COMMENT ON COLUMN "PAY_SYSTEM"."CUSTOMERS"."CUSTOMER_ID" IS 'Primary key of the customers table.';
--------------------------------------------------------
--  DDL for Table PAYMENT_AUDITS
--------------------------------------------------------

  CREATE TABLE "PAY_SYSTEM"."PAYMENT_AUDITS" 
   (
    "AUDIT_ID" NUMBER(19,0), 
	"BALANCE" NUMBER, 
	"AMOUNT" NUMBER, 
	"STATUS" VARCHAR2(10)
   );

   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENT_AUDITS"."BALANCE" IS 'Account''s balance before payment''s tx.';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENT_AUDITS"."AUDIT_ID" IS 'Primary key of the payment_audits table';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENT_AUDITS"."AMOUNT" IS 'Amount of the operation';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENT_AUDITS"."STATUS" IS 'Status of the operation';
--------------------------------------------------------
--  DDL for Table PAYMENT_ORDERS
--------------------------------------------------------

  CREATE TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" 
   (
    "ORDER_ID" NUMBER(19,0),
    "AMOUNT" NUMBER, 
	"TYPE" VARCHAR2(10),  
	"CREATED_DATE" DATE, 
	"DEPOSIT_ACCOUNT_ID" NUMBER(19,0), 
	"WITHDRAW_ACCOUNT_ID" NUMBER(19,0), 
	"CUSTOMER_ID" NUMBER(19,0)
   );

   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENT_ORDERS"."TYPE" IS 'Payment order''s type';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENT_ORDERS"."ORDER_ID" IS 'Specific number of the order';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENT_ORDERS"."DEPOSIT_ACCOUNT_ID" IS 'Deposit account number of account which takes money';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENT_ORDERS"."WITHDRAW_ACCOUNT_ID" IS 'Withdraw account number of account with gives money';
--------------------------------------------------------
--  DDL for Table PAYMENTS
--------------------------------------------------------

  CREATE TABLE "PAY_SYSTEM"."PAYMENTS" 
   (
    "PAYMENT_ID" NUMBER(19,0), 
	"AMOUNT" NUMBER, 
	"STATUS" VARCHAR2(10), 
    "AUDIT_ID" NUMBER(19,0),
	"PYMT_ORDER_ID" NUMBER(19,0), 
	"REG_PAYMENT_ID" NUMBER(19,0), 
	"DEPOSIT_ACCOUNT_ID" NUMBER(19,0), 
	"WITHDRAW_ACCOUNT_ID" NUMBER(19,0)
   );

   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENTS"."PAYMENT_ID" IS 'Primary key of the payments table';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENTS"."PYMT_ORDER_ID" IS 'Payment order id of payment order';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENTS"."DEPOSIT_ACCOUNT_ID" IS 'Deposit account number of account which takes money';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENTS"."WITHDRAW_ACCOUNT_ID" IS 'Withdraw account number of account which gives money';
   COMMENT ON COLUMN "PAY_SYSTEM"."PAYMENTS"."AUDIT_ID" IS 'Audit id of payment audit';
--------------------------------------------------------
--  DDL for Table REGULAR_PAYMENTS
--------------------------------------------------------

  CREATE TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" 
   (
    "REG_PAYMENT_ID" NUMBER(19,0), 
	"START_DATE" DATE, 
	"END_DATE" DATE, 
    "AMOUNT" NUMBER(19,0),
    "RATE" VARCHAR2(10), 
	"STATUS" VARCHAR2(10),
	"DEPOSIT_ACCOUNT_ID" NUMBER(19,0), 
	"WITHDRAW_ACCOUNT_ID" NUMBER(19,0),
	"PYMT_ORDER_ID" NUMBER(19,0)
   );

   COMMENT ON COLUMN "PAY_SYSTEM"."REGULAR_PAYMENTS"."RATE" IS 'Regular payment''s frequency';
   COMMENT ON COLUMN "PAY_SYSTEM"."REGULAR_PAYMENTS"."REG_PAYMENT_ID" IS 'Primary key of the regular payments table';
   COMMENT ON COLUMN "PAY_SYSTEM"."REGULAR_PAYMENTS"."DEPOSIT_ACCOUNT_ID" IS 'Deposit account number of account which takes money';
   COMMENT ON COLUMN "PAY_SYSTEM"."REGULAR_PAYMENTS"."PYMT_ORDER_ID" IS 'Payment order id of payment order';
   COMMENT ON COLUMN "PAY_SYSTEM"."REGULAR_PAYMENTS"."WITHDRAW_ACCOUNT_ID" IS 'Withdraw account number of account with gives money';
--------------------------------------------------------
--  DDL for Table TRANSACTIONS
--------------------------------------------------------

  CREATE TABLE "PAY_SYSTEM"."TRANSACTIONS" 
   (
    "TX_ID" NUMBER(19,0), 
	"AMOUNT" NUMBER,
    "PAYMENT_ID" NUMBER(19,0),
	"DEPOSIT_ACCOUNT_ID" NUMBER(19,0), 
	"WITHDRAW_ACCOUNT_ID" NUMBER(19,0)
   );

   COMMENT ON COLUMN "PAY_SYSTEM"."TRANSACTIONS"."TX_ID" IS 'Primary key of transactions table';
   COMMENT ON COLUMN "PAY_SYSTEM"."TRANSACTIONS"."DEPOSIT_ACCOUNT_ID" IS 'Deposit account number of account which takes money';
   COMMENT ON COLUMN "PAY_SYSTEM"."TRANSACTIONS"."PAYMENT_ID" IS 'Payment id of payment';
   COMMENT ON COLUMN "PAY_SYSTEM"."TRANSACTIONS"."WITHDRAW_ACCOUNT_ID" IS 'Withdraw account number of account with gives money';
--------------------------------------------------------
--  DDL for Sequence ACCOUNT_BALANCES_SEQ
--------------------------------------------------------

CREATE SEQUENCE "PAY_SYSTEM".account_balances_sqe
 START WITH     1
 INCREMENT BY   1
 MINVALUE 1
 NOCACHE
 NOCYCLE;   
 --------------------------------------------------------
--  DDL for Sequence ACCOUNTS_SEQ
--------------------------------------------------------

CREATE SEQUENCE "PAY_SYSTEM".accounts_sqe
 START WITH     1
 INCREMENT BY   1
 MINVALUE 1
 NOCACHE
 NOCYCLE;   
  --------------------------------------------------------
--  DDL for Sequence TRANSACTIONS_SEQ
--------------------------------------------------------

CREATE SEQUENCE "PAY_SYSTEM".transactions_sqe
 START WITH     1
 INCREMENT BY   1
 MINVALUE 1
 NOCACHE
 NOCYCLE; 
  --------------------------------------------------------
--  DDL for Sequence CUSTOMERS_SEQ
--------------------------------------------------------

CREATE SEQUENCE "PAY_SYSTEM".customers_sqe
 START WITH     1
 INCREMENT BY   1
 MINVALUE 1
 NOCACHE
 NOCYCLE; 
 --------------------------------------------------------
--  DDL for Sequence PAYMENT_AUDITS_SEQ
--------------------------------------------------------

CREATE SEQUENCE "PAY_SYSTEM".payment_audits_sqe
 START WITH     1
 INCREMENT BY   1
 MINVALUE 1
 NOCACHE
 NOCYCLE; 
 --------------------------------------------------------
--  DDL for Sequence PAYMENT_ORDERS_SEQ
--------------------------------------------------------

CREATE SEQUENCE "PAY_SYSTEM".payment_orders_sqe
 START WITH     1
 INCREMENT BY   1
 MINVALUE 1
 NOCACHE
 NOCYCLE; 
 --------------------------------------------------------
--  DDL for Sequence PAYMENTS_SEQ
--------------------------------------------------------

CREATE SEQUENCE "PAY_SYSTEM".payments_sqe
 START WITH     1
 INCREMENT BY   1
 MINVALUE 1
 NOCACHE
 NOCYCLE; 
 --------------------------------------------------------
--  DDL for Sequence REGULAR_PAYMENTS_SEQ
--------------------------------------------------------

CREATE SEQUENCE "PAY_SYSTEM".regular_payments_sqe
 START WITH     1
 INCREMENT BY   1
 MINVALUE 1
 NOCACHE
 NOCYCLE; 
--------------------------------------------------------
--  DDL for Index ACC_BAL_ID_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."ACC_BAL_ID_PK" ON "PAY_SYSTEM"."ACCOUNT_BALANCES" ("BALANCE_ID");
--------------------------------------------------------
--  DDL for Index ACC_BAL_ACCOUNT_ID_IX
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."ACC_BAL_ACCOUNT_ID_IX" ON "PAY_SYSTEM"."ACCOUNT_BALANCES" ("ACCOUNT_ID");
--------------------------------------------------------
--  DDL for Index ACC_ACCOUNT_ID_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."ACCOUNT_ID_PK" ON "PAY_SYSTEM"."ACCOUNTS" ("ACCOUNT_ID");
--------------------------------------------------------
--  DDL for Index ACC_ACCOUNT_ID_CUSTOMER_ID_IX
--------------------------------------------------------

  CREATE INDEX "PAY_SYSTEM"."ACC_CUSTOMER_ID_IX" 
  ON "PAY_SYSTEM"."ACCOUNTS" ("CUSTOMER_ID");
--------------------------------------------------------
--  DDL for Index CSTM_CSTM_ID_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."CSTM_ID_PK" ON "PAY_SYSTEM"."CUSTOMERS" ("CUSTOMER_ID");
--------------------------------------------------------
--  DDL for Index PYMT_AUDITS_PYMT_AUDIT_ID_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."PYMT_AUDITS_PYMT_AUDIT_ID_PK" ON 
  "PAY_SYSTEM"."PAYMENT_AUDITS" ("AUDIT_ID");
--------------------------------------------------------
--  DDL for Index PYMT_ORDERS_ORDER_ID_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."PYMT_ORDERS_ORDER_ID_PK" 
  ON "PAY_SYSTEM"."PAYMENT_ORDERS" ("ORDER_ID") ;
--------------------------------------------------------
--  DDL for Index PYMT_ORDERS_DEPOSIT_ACC_WITHDRAW_ACC_IX
--------------------------------------------------------

  CREATE INDEX "PAY_SYSTEM"."PYMT_ORDERS_DEPOSIT_ACC_WITHDRAW_ACC_IX" ON
  "PAY_SYSTEM"."PAYMENT_ORDERS" ("DEPOSIT_ACCOUNT_ID", "WITHDRAW_ACCOUNT_ID");
--------------------------------------------------------
--  DDL for Index PYMT_ORDERS_ORDER_NUMBER_CREATED_DATE_IX
--------------------------------------------------------

  CREATE INDEX "PAY_SYSTEM"."PYMT_ORDERS_CREATED_DATE_IX" ON 
  "PAY_SYSTEM"."PAYMENT_ORDERS" ("CREATED_DATE");
--------------------------------------------------------
--  DDL for Index PYMT_ORDERS_CUSTOMER_IX
--------------------------------------------------------

  CREATE INDEX "PAY_SYSTEM"."PYMT_ORDERS_CUSTOMER_IX" ON 
  "PAY_SYSTEM"."PAYMENT_ORDERS" ("CUSTOMER_ID");
--------------------------------------------------------
--  DDL for Index PYMT_PYMT_ID_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."PYMT_ID_PK" ON 
  "PAY_SYSTEM"."PAYMENTS" ("PAYMENT_ID");
--------------------------------------------------------
--  DDL for Index PYMT_PYMT_AUDIT_UK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."PYMT_PYMT_AUDIT_UK" ON 
  "PAY_SYSTEM"."PAYMENTS" ("AUDIT_ID");
--------------------------------------------------------
--  DDL for Index PYMT_PYMT_ORDER_UK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."PYMT_PYMT_ORDER_UK" ON
  "PAY_SYSTEM"."PAYMENTS" ("PYMT_ORDER_ID");
--------------------------------------------------------
--  DDL for Index PYMT_REG_PAYMENT_IX
--------------------------------------------------------

  CREATE INDEX "PAY_SYSTEM"."PYMT_REG_PAYMENT_IX" ON 
  "PAY_SYSTEM"."PAYMENTS" ("REG_PAYMENT_ID");
--------------------------------------------------------
--  DDL for Index PYMT_DEPOSIT_ACC_WITHDRAW_ACC_IX
--------------------------------------------------------

  CREATE INDEX "PAY_SYSTEM"."PYMT_DEPOSIT_ACC_WITHDRAW_ACC_IX" ON 
  "PAY_SYSTEM"."PAYMENTS" ("DEPOSIT_ACCOUNT_ID", "WITHDRAW_ACCOUNT_ID");
--------------------------------------------------------
--  DDL for Index REG_PYMT_REG_PYMT_ID_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."REG_PYMT_ID_PK" ON
  "PAY_SYSTEM"."REGULAR_PAYMENTS" ("REG_PAYMENT_ID");
  --------------------------------------------------------
--  DDL for Index REG_PYMT_START_DATE_END_DATE_IX
--------------------------------------------------------

  CREATE INDEX "PAY_SYSTEM"."REG_PYMT_START_DATE_END_DATE_IX" ON 
  "PAY_SYSTEM"."REGULAR_PAYMENTS" ("START_DATE", "END_DATE");
--------------------------------------------------------
--  DDL for Index PYMT_DEPOSIT_ACC_WITHDRAW_ACC_IX
--------------------------------------------------------

  CREATE INDEX "PAY_SYSTEM"."REG_PYMT_DEPOSIT_ACC_WITHDRAW_ACC_IX" ON 
  "PAY_SYSTEM"."REGULAR_PAYMENTS" ("DEPOSIT_ACCOUNT_ID", "WITHDRAW_ACCOUNT_ID");
--------------------------------------------------------
--  DDL for Index REG_PYMT_PO_ORDER_UK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."REG_PYMT_PYMT_ORDER_UK" ON 
  "PAY_SYSTEM"."REGULAR_PAYMENTS" ("PYMT_ORDER_ID");
--------------------------------------------------------
--  DDL for Index TX_ID_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."TX_ID_PK" ON 
  "PAY_SYSTEM"."TRANSACTIONS" ("TX_ID");
--------------------------------------------------------
--  DDL for Index TX_DEPOSIT_ACC_WITHDRAW_ACC_IX
--------------------------------------------------------

  CREATE INDEX "PAY_SYSTEM"."TX_DEPOSIT_ACC_WITHDRAW_ACC_IX" 
  ON "PAY_SYSTEM"."TRANSACTIONS" ("DEPOSIT_ACCOUNT_ID", "WITHDRAW_ACCOUNT_ID");
--------------------------------------------------------
--  DDL for Index TX_PAYMENT_UK
--------------------------------------------------------

  CREATE UNIQUE INDEX "PAY_SYSTEM"."TX_PAYMENT_UK" ON 
  "PAY_SYSTEM"."TRANSACTIONS" ("PAYMENT_ID");
   --------------------------------------------------------
--  Constraints for Table ACCOUNT_BALANCES
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."ACCOUNT_BALANCES" 
  MODIFY ("BALANCE_ID" NOT NULL);
  ALTER TABLE "PAY_SYSTEM"."ACCOUNT_BALANCES" 
  MODIFY ("AMOUNT" NOT NULL);
  ALTER TABLE "PAY_SYSTEM"."ACCOUNT_BALANCES"
  MODIFY ("ACCOUNT_ID" NOT NULL);
  ALTER TABLE "PAY_SYSTEM"."ACCOUNT_BALANCES"
  ADD CONSTRAINT "ACC_BAL_ID_PK" PRIMARY KEY ("BALANCE_ID")
  USING INDEX "PAY_SYSTEM".ACC_BAL_ID_PK ENABLE;
  ALTER TABLE "PAY_SYSTEM"."ACCOUNT_BALANCES" 
  ADD CONSTRAINT "ACC_BAL_ACCOUNT_UK" UNIQUE ("ACCOUNT_ID")
  USING INDEX "PAY_SYSTEM".ACC_BAL_ACCOUNT_ID_IX ENABLE;
--------------------------------------------------------
--  Constraints for Table ACCOUNTS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."ACCOUNTS" MODIFY ("ACCOUNT_ID" NOT NULL);
  ALTER TABLE "PAY_SYSTEM"."ACCOUNTS" MODIFY ("CUSTOMER_ID" NOT NULL);
  ALTER TABLE "PAY_SYSTEM"."ACCOUNTS" ADD CONSTRAINT "ACCOUNT_ID_PK" 
  PRIMARY KEY ("ACCOUNT_ID") USING INDEX  "PAY_SYSTEM".ACCOUNT_ID_PK ENABLE;
--------------------------------------------------------
--  Constraints for Table CUSTOMERS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."CUSTOMERS" MODIFY ("CUSTOMER_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."CUSTOMERS" MODIFY ("FIRST_NAME" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."CUSTOMERS" MODIFY ("LAST_NAME" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."CUSTOMERS" MODIFY ("EMAIL" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."CUSTOMERS" MODIFY ("TEL" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."CUSTOMERS" ADD CONSTRAINT "CSTM_ID_PK" PRIMARY KEY ("CUSTOMER_ID")
  USING INDEX  "PAY_SYSTEM".CSTM_ID_PK ENABLE;
--------------------------------------------------------
--  Constraints for Table PAYMENT_AUDITS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."PAYMENT_AUDITS" MODIFY ("AUDIT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_AUDITS" MODIFY ("BALANCE" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_AUDITS" MODIFY ("AMOUNT" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_AUDITS" MODIFY ("STATUS" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_AUDITS" ADD CONSTRAINT "PYMT_AUDITS_ID_PK" 
  PRIMARY KEY ("AUDIT_ID") USING INDEX "PAY_SYSTEM".PYMT_AUDITS_PYMT_AUDIT_ID_PK ENABLE;
--------------------------------------------------------
--  Constraints for Table PAYMENT_ORDERS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" MODIFY ("ORDER_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" MODIFY ("CREATED_DATE" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" MODIFY ("AMOUNT" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" MODIFY ("TYPE" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" MODIFY ("CUSTOMER_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" MODIFY ("DEPOSIT_ACCOUNT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" MODIFY ("WITHDRAW_ACCOUNT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" ADD CONSTRAINT "PYMT_ORDERS_ID_PK"
  PRIMARY KEY ("ORDER_ID") USING INDEX "PAY_SYSTEM".PYMT_ORDERS_ORDER_ID_PK ENABLE;
--------------------------------------------------------
--  Constraints for Table PAYMENTS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" MODIFY ("PAYMENT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" MODIFY ("AMOUNT" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" MODIFY ("STATUS" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" MODIFY ("AUDIT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" MODIFY ("PYMT_ORDER_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" MODIFY ("DEPOSIT_ACCOUNT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" MODIFY ("WITHDRAW_ACCOUNT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" ADD CONSTRAINT "PYMT_ID_PK" PRIMARY KEY ("PAYMENT_ID")
  USING INDEX  "PAY_SYSTEM".PYMT_ID_PK ENABLE;
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" ADD CONSTRAINT "PYMT_PYMT_AUDIT_UK" UNIQUE ("AUDIT_ID")
  USING INDEX "PAY_SYSTEM"."PYMT_PYMT_AUDIT_UK" ENABLE;
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" ADD CONSTRAINT "PYMT_PYMT_ORDER_UK" UNIQUE ("PYMT_ORDER_ID")
  USING INDEX "PAY_SYSTEM"."PYMT_PYMT_ORDER_UK" ENABLE;
--------------------------------------------------------
--  Constraints for Table REGULAR_PAYMENTS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" MODIFY ("RATE" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" MODIFY ("REG_PAYMENT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" MODIFY ("START_DATE" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" MODIFY ("END_DATE" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" MODIFY ("STATUS" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" MODIFY ("AMOUNT" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" MODIFY ("PYMT_ORDER_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" MODIFY ("DEPOSIT_ACCOUNT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" MODIFY ("WITHDRAW_ACCOUNT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" ADD CONSTRAINT "REG_PYMT_ID_PK" 
  PRIMARY KEY ("REG_PAYMENT_ID") USING INDEX "PAY_SYSTEM".REG_PYMT_ID_PK ENABLE;
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" ADD CONSTRAINT "REG_PYMT_PYMT_ORDER_UK" UNIQUE ("PYMT_ORDER_ID")
  USING INDEX "PAY_SYSTEM"."REG_PYMT_PYMT_ORDER_UK" ENABLE;
--------------------------------------------------------
--  Constraints for Table TRANSACTIONS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."TRANSACTIONS" MODIFY ("TX_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."TRANSACTIONS" MODIFY ("AMOUNT" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."TRANSACTIONS" MODIFY ("DEPOSIT_ACCOUNT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."TRANSACTIONS" MODIFY ("WITHDRAW_ACCOUNT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."TRANSACTIONS" MODIFY ("PAYMENT_ID" NOT NULL );
  ALTER TABLE "PAY_SYSTEM"."TRANSACTIONS" ADD CONSTRAINT "TX_ID_PK" PRIMARY KEY ("TX_ID")
  USING INDEX  "PAY_SYSTEM".TX_ID_PK ENABLE;
  ALTER TABLE "PAY_SYSTEM"."TRANSACTIONS" ADD CONSTRAINT "TX_PAYMENT_UK" UNIQUE ("PAYMENT_ID")
  USING INDEX  "PAY_SYSTEM"."TX_PAYMENT_UK" ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table ACCOUNT_BALANCES
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."ACCOUNT_BALANCES" ADD CONSTRAINT "ACC_BAL_ACCOUNT_FK" FOREIGN KEY ("ACCOUNT_ID")
	  REFERENCES "PAY_SYSTEM"."ACCOUNTS" ("ACCOUNT_ID") ;
--------------------------------------------------------
--  Ref Constraints for Table ACCOUNTS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."ACCOUNTS" ADD CONSTRAINT "ACC_CUSTOMER_FK" FOREIGN KEY ("CUSTOMER_ID")
	  REFERENCES "PAY_SYSTEM"."CUSTOMERS" ("CUSTOMER_ID") ;
--------------------------------------------------------
--  Ref Constraints for Table PAYMENT_ORDERS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" ADD CONSTRAINT "PYMT_ORDERS_WITHDRAW_ACCOUNT_FK"
  FOREIGN KEY ("WITHDRAW_ACCOUNT_ID") REFERENCES "PAY_SYSTEM"."ACCOUNTS" ("ACCOUNT_ID") ;
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" ADD CONSTRAINT "PYMT_ORDERS_DEPOSIT_ACCOUNT_FK"
  FOREIGN KEY ("DEPOSIT_ACCOUNT_ID")REFERENCES "PAY_SYSTEM"."ACCOUNTS" ("ACCOUNT_ID") ;
  ALTER TABLE "PAY_SYSTEM"."PAYMENT_ORDERS" ADD CONSTRAINT "PYMT_ORDERS_CUSTOMER_FK" 
  FOREIGN KEY ("CUSTOMER_ID") REFERENCES "PAY_SYSTEM"."CUSTOMERS" ("CUSTOMER_ID") ;
--------------------------------------------------------
--  Ref Constraints for Table PAYMENTS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" ADD CONSTRAINT "PYMT_WITHDRAW_ACCOUNT_FK" 
  FOREIGN KEY ("WITHDRAW_ACCOUNT_ID") REFERENCES "PAY_SYSTEM"."ACCOUNTS" ("ACCOUNT_ID") ;
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" ADD CONSTRAINT "PYMT_DEPOSIT_ACCOUNT_FK" 
  FOREIGN KEY ("DEPOSIT_ACCOUNT_ID") REFERENCES "PAY_SYSTEM"."ACCOUNTS" ("ACCOUNT_ID") ;
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" ADD CONSTRAINT "PYMT_AUDIT_FK" 
  FOREIGN KEY ("AUDIT_ID") REFERENCES "PAY_SYSTEM"."PAYMENT_AUDITS" ("AUDIT_ID") ;
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" ADD CONSTRAINT "PYMT_ORDER_FK"
  FOREIGN KEY ("PYMT_ORDER_ID") REFERENCES "PAY_SYSTEM"."PAYMENT_ORDERS" ("ORDER_ID");
  ALTER TABLE "PAY_SYSTEM"."PAYMENTS" ADD CONSTRAINT "PYMT_REG_PYMT_FK" 
  FOREIGN KEY ("REG_PAYMENT_ID") REFERENCES "PAY_SYSTEM"."REGULAR_PAYMENTS" ("REG_PAYMENT_ID") ;
--------------------------------------------------------
--  Ref Constraints for Table REGULAR_PAYMENTS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" ADD CONSTRAINT "REG_PYMT_WITHDRAW_ACC_FK"
  FOREIGN KEY ("WITHDRAW_ACCOUNT_ID") REFERENCES "PAY_SYSTEM"."ACCOUNTS" ("ACCOUNT_ID") ;
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" ADD CONSTRAINT "REG_PYMT_DEPOSIT_ACC_FK"
  FOREIGN KEY ("DEPOSIT_ACCOUNT_ID") REFERENCES "PAY_SYSTEM"."ACCOUNTS" ("ACCOUNT_ID") ;
  ALTER TABLE "PAY_SYSTEM"."REGULAR_PAYMENTS" ADD CONSTRAINT "REG_PYMT_PYMT_ORDER_FK"
  FOREIGN KEY ("PYMT_ORDER_ID") REFERENCES "PAY_SYSTEM"."PAYMENT_ORDERS" ("ORDER_ID") ;
--------------------------------------------------------
--  Ref Constraints for Table TRANSACTIONS
--------------------------------------------------------

  ALTER TABLE "PAY_SYSTEM"."TRANSACTIONS" ADD CONSTRAINT "TX_PAYMENT_FK" FOREIGN KEY ("PAYMENT_ID")
	  REFERENCES "PAY_SYSTEM"."PAYMENTS" ("PAYMENT_ID") ;
    